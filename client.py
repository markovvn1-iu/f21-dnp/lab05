import os
import argparse
from typing import Any, List, Optional, Union
from xmlrpc.client import ServerProxy


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='RPC client (D&NP lab05)',
        add_help=True,
    )

    parser.add_argument('addr', type=str, metavar='ADDR', help='port for start server')
    parser.add_argument('port', type=int, metavar='PORT', help='port for start server')
    return parser.parse_args(args)


class NotCompleteError(Exception):
    pass


class Controller:

    _proxy: ServerProxy

    def __init__(self, addr: str, port: int):
        self._proxy = ServerProxy(f'http://{addr}:{port}')

    def _call_rpc(self, method: str, *args: Any, **kwargs: Any) -> Any:
        status: bool
        res: Union[str, List[str]]
        status, res = self._proxy.__getattr__(method)(*args, **kwargs)
        if not status:
            raise NotCompleteError(res)
        return res

    def send(self, filename: str):
        try:
            with open(filename, 'rb') as f:
                buff = f.read()                
        except FileNotFoundError:
            raise NotCompleteError('No such file')

        self._call_rpc('send_file', filename, buff)

    def list(self) -> bool:
        res: List[str] = self._call_rpc('list_files')
        print('\n'.join(res))

    def delete(self, filename: str):
        self._call_rpc('delete_file', filename)

    def get(self, filename: str, new_filename: str) -> bool:
        if os.path.isfile(new_filename):
            raise NotCompleteError('File already exists')
        buff: bytes = self._call_rpc('get_file', filename)
        with open(new_filename, 'wb') as f:
            f.write(buff.data)

    def calc(self, expression: str) -> bool:
        res: str = self._call_rpc('calculate', expression)
        print(res)

    def __call__(self, cmd: str, args: str) -> bool:
        args_splited = args.split()

        if cmd == 'send' and len(args_splited) == 1:
            return self.send(args_splited[0])
        if cmd == 'list' and len(args_splited) == 0:
            return self.list()
        if cmd == 'delete' and len(args_splited) == 1:
            return self.delete(args_splited[0])
        if cmd == 'get' and len(args_splited) in [1, 2]:
            return self.get(args_splited[0], args_splited[1] if len(args_splited) == 2 else args_splited[0])
        if cmd == 'calc' and len(args) > 0:
            return self.calc(args)

        raise NotCompleteError('Wrong command')


def main():
    args = parse_args()

    controller = Controller(args.addr, args.port)

    try:
        while True:
            line = input('Enter the command: ').strip()
            cmd = line.split()[0]
            args = line[len(cmd):].strip()

            if not cmd:
                print('Wrong command\n')
                continue
            if cmd == 'quit':
                break

            try:
                controller(cmd, args)
                print('Completed')
            except NotCompleteError as e:
                print('Not completed')
                print(e)
            print()
    except KeyboardInterrupt:
        print()
    finally:
        print('Client is stopping')


if __name__ == '__main__':
    main()
