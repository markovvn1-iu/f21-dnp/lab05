from genericpath import isfile
import os
import argparse
from typing import Any, Callable, List, Optional, Tuple, Union
from xmlrpc.client import Binary
from xmlrpc.server import SimpleXMLRPCServer


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='RPC server (D&NP lab05)',
        add_help=True,
    )

    parser.add_argument('addr', type=str, metavar='ADDR', help='port for start server')
    parser.add_argument('port', type=int, metavar='PORT', help='port for start server')
    return parser.parse_args(args)


class WrongExpression(Exception):
    pass


class CalcController:

    calc_func = {
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b,
        '-': lambda a, b: a - b,
        '+': lambda a, b: a + b,
        '>': lambda a, b: a > b,
        '<': lambda a, b: a < b,
        '>=': lambda a, b: a >= b,
        '<=': lambda a, b: a <= b,
    }

    def _parse_number(self, number_str: str) -> Union[int, float]:
        try:
            return int(number_str)
        except ValueError:
            pass

        try:
            return float(number_str)
        except ValueError:
            pass

        raise WrongExpression(f"Cannot cast '{number_str}' to int or to float")
            

    def __call__(self, expression: str) -> str:
        data = expression.strip().split()
        if len(data) != 3:
            raise WrongExpression('There are more than 3 items')
        
        cmd = data[0]
        if cmd not in self.calc_func:
            raise WrongExpression(f"Unknown operand '{cmd}'")

        a = self._parse_number(data[1])
        b = self._parse_number(data[2])

        res = self.calc_func[cmd](a, b)

        return str(res)


def safe_rpc_method(func: Callable[..., Tuple[bool, str]]):
    def wrap(*args: Any, **kwargs: Any) -> Any:
        try:
            return func(*args, **kwargs)
        except BaseException as e:
            return False, repr(e)
    return wrap

class Controller:

    _storage_path: str
    _calc_controller: CalcController

    def __init__(self, storage_path: str):
        self._storage_path = storage_path
        self._calc_controller = CalcController()

    @safe_rpc_method
    def send_file(self, filename: str, data: Binary) -> Tuple[bool, str]:
        full_path = os.path.join(self._storage_path, filename)
        if os.path.isfile(full_path):
            print(f'{filename} not saved')
            return False, 'File already exists'
        with open(full_path, 'wb') as f:
            f.write(data.data)
        print(f'{filename} saved')
        return True, ''

    @safe_rpc_method
    def list_files(self) -> Tuple[bool, List[str]]:
        files = os.listdir(self._storage_path)
        return True, [f for f in files if os.path.isfile(f)]

    @safe_rpc_method
    def delete_file(self, filename: str) -> Tuple[bool, str]:
        try:
            os.remove(os.path.join(self._storage_path, filename))
        except FileNotFoundError:
            print(f'{filename} not deleted')
            return False, 'No such file'
        print(f'{filename} deleted')
        return True, ''

    @safe_rpc_method
    def get_file(self, filename: str) -> Tuple[bool, Union[bytes, str]]:
        try:
            with open(os.path.join(self._storage_path, filename), 'rb') as f:
                buff = f.read()
        except (FileNotFoundError, PermissionError, IsADirectoryError):
            print(f'No such file: {filename}')
            return False, 'No such file'
        print(f'File send: {filename}')
        return True, buff

    @safe_rpc_method
    def calculate(self, expression: str) -> Tuple[bool, str]:
        try:
            res = self._calc_controller(expression)
        except ZeroDivisionError:
            print(f'{expression} -- not done')
            return False, 'Division by zero'
        except WrongExpression:
            print(f'{expression} -- not done')
            return False, 'Wrong expression'
        print(f'{expression} -- done')
        return True, res



def main():
    args = parse_args()
    
    os.makedirs('storage', exist_ok=True)
    controller = Controller('storage')

    # Create server
    with SimpleXMLRPCServer((args.addr, args.port), logRequests=False) as server:
        server.register_introspection_functions()

        # Register instance for the controller
        server.register_instance(controller)

        # Run the server's main loop
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            print()
        finally:
            print('Server is stopping')


if __name__ == '__main__':
    main()
    
